### Summary
A simple D&D 5 edition app.

Mostly focusing on an interactive character sheet, might do something more later idk.

The project is mostly HTML & Javascript with minimal CSS for styling.

The app uses as a back end a project of this beautiful [user](https://github.com/adrpadua) which is hosted on [D&D 5e api](http://www.dnd5eapi.co/).

### Till Now:
*	Base character stats as name, level, proficiency, hit points.
*	Support of main classes and races.
*	Ability, Saving and Skill tables.
*	Main languages are supported too.
*	Items including weapons, equipment and utility.
*	Feats that a character of a given level and class should have.
*	Table of known spells/cantrips.
*	Download, Load or Save to server a character sheet from a file on the device, and export/import as plain text.

### WIP:
*   Dialog for skill proficiencies choose when a given class is selected.
*   Dialog for optional starting equipment for classes.
*   Support for automatically adding proficiency to proficient items (weapons, tool kits, etc.).