class DnDCanvas{
    constructor(canvas){
        //How much pixels to display for each cell on screen.
        this.visual_unit=10;
        //How much pixels per a game unit.
        this.game_unit=10;

        this.items=[];
        this.selected=[];

        this.size={
            x:canvas.width,
            y:canvas.height
        };

        this.context=canvas.getContext("2d");
    }

    Add(item){
        this.items.push(item);
    }
    Remove(item){
        this.items=this.items.filter((x)=>x!==item);
    }
    Select(item){
        if(!this.items.includes(item)){
            this.Add(item);
        }

        if(!this.selected.includes(item)){
            this.selected.push(item);
        }else{
            this.selected=this.selected.filter((x)=>x!==item);
        }
    }

    Clear(){
        this.context.clearRect(0,0,this.size.x,this.size.y);
    }
    Draw(){
        for(let i=0;i<this.items.length;i+=1){
            this.items[i].Draw(this.context);
        }
        this.DrawMark();
    }
    DrawMark(){
        let item;
        let oldStyle=this.context.fillStyle;
        let oldAlpha=this.context.globalAlpha;

        this.context.fillStyle='blue';
        this.context.globalAlpha=0.6;
        
        for(let i=0;i<this.selected.length;i+=1){
            item=this.selected[i];
            this.context.fillRect(item.position.x,item.position.y,item.size.x,item.size.y);
        }

        this.context.fillStyle=oldStyle;
        this.context.globalAlpha=oldAlpha;
    }

    Move(x,y){
        for(let i=0;i<this.selected.length;i+=1){
            this.items[i].MoveWith(x,y);
        }
        this.Draw();
    }
    DrawGrid(){
        for (let x = 0; x <= this.size.x; x += this.visual_unit) {
            this.context.moveTo(0.5 + x, 0);
            this.context.lineTo(0.5 + x, this.size.y);
        }
        
        for (let x = 0; x <= this.size.y; x += this.visual_unit) {
            this.context.moveTo(0, 0.5 + x);
            this.context.lineTo(this.size.x, 0.5 + x);
        }
        this.context.strokeStyle = "gray";
        this.context.stroke();
    }
}

class CanvasItem{
    constructor(){
        this.static=false;
        this.points=[];
        this.image={
            url:undefined,
            src:undefined
        };

        this.name="";

        this.rotation=0;
        this.position={
            x:0,
            y:0
        };
        this.size={
            x:1,
            y:1
        };
    }

    Draw(context){
        if(this.image.url===undefined && this.image.src===undefined){
            context.beginPath();
            context.moveTo(this.points[0].x, this.points[0].y);
            for(let i=0;i<this.points.length;i+=1){
                context.lineTo(this.points[i].x, this.points[i].y);
            }
            context.closePath();
        }else{
            let image=new Image();
            /*image.addEventListener("load",function(){
                
            });*/
            if(this.image.url!==undefined){
                image.src=this.image.url;
            }else{
                image.src=this.image.src;
            }
            context.drawImage(image,this.position.x,this.position.y,this.size.x,this.size.y);
        }
    }
    MoveTo(x,y){
        if(this.static){
            return;
        }
    
        this.position.x=x;
        this.position.y=y;
    }
    MoveWith(x,y){
        if(this.static){
            return;
        }
    
        this.position.x=parseInt(this.position.x)+parseInt(x);
        this.position.y=parseInt(this.position.y)+parseInt(y);
    }
}