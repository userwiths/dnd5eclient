function LoadCharacters(){
    let files=document.getElementById("load").files;
    let local=JSON.parse(localStorage.getItem("character"));
    let host=localStorage.getItem("host");

    for(let i=0;i<files.length;i+=1){
        LoadFromFile(files[i]);
    }
    if(local!==undefined && local!==null){
        LoadCharacter(local);
        SaveToServer(local).fail((err)=>{
            toastr.success("Failed to save character to server. Click to save as file.");

		    toastr.options = notifier_options;
		    toastr.options.onclick=function(){
			    SaveCharacters();
            };
        });
        localStorage.removeItem("character");
    }
    if(host!==null){
        LoadFromServer();
    }
}
function HasUnsaved(){
    let local=JSON.parse(localStorage.getItem("character"));
    if(local===null){
        return;
    }

    let cell=document.querySelector("#"+local.id);
    if(cell===null){
        window.location.reload();
    }
}
function LoadFromFile(file){
    let reader = new FileReader();
    reader.onload = function(){
        let text = reader.result;
        LoadCharacter(JSON.parse(text));
    };

    reader.readAsText(file);
}
function LoadFromServer(){
    let host=localStorage.getItem("host");
	$.ajax({
		url: "http://"+host+"/characters",
		type: 'GET',
		dataType: 'json'
	}).done((characters)=>{
        for(let i=0;i<characters.length;i+=1){
            LoadCharacter(characters[i]);
        }
    }).fail(generalError);
}

function SaveToServer(character){
	let host=localStorage.getItem("host");
	return $.ajax({
		url: "http://"+host+"/characters",
		type: 'POST',
		dataType: 'json',
		data:character
	}).fail((error)=>console.error("Could not save character to server."));
}

function SaveCharacters(){
    localStorage.clear();

    let elements=document.querySelectorAll(".character");
    let result=[];

    for(let i=0;i<elements.length;i+=1){
        result.push(JSON.parse(elements[i].value));
        console.log(result[result.length-1].name);
    }

    let blob=new Blob([JSON.stringify(result)],{type : 'application/json'});
	let url = window.URL.createObjectURL(blob);
	let a = document.createElement('a');
    
    a.style.display = 'none';
	a.href = url;
	a.download = 'characters.json';

    document.body.appendChild(a);
	a.click();
	window.URL.revokeObjectURL(url);
}

function LoadCharacter(character){
    let element=document.getElementById("characters");
    let cell=document.querySelector("#"+character.name.normalizeName());
    
    if(cell===null){
        cell=document.createElement("div");
    }else{
        RemoveChildrenOf("#"+character.name.normalizeName());
    }

    let input=document.createElement("input");
    
    let avatar=document.createElement("img");

    input.type="hidden";
    input.id=character.name.normalizeName();
    input.classList.add("character");
    input.value=JSON.stringify(character);

    avatar.className="avatar";
    avatar.src=character.avatar;

    cell.classList.add("character-plate");
    //cell.id=character.name.normalizeName();

    cell.appendChild(avatar);
    cell.appendChild(input);
    cell.appendChild(LoadOptions(character));
    cell.appendChild(LoadCharacterStats(character));

    element.appendChild(cell);
}
function LoadOptions(character){
    let div=document.createElement("div");
    let expand=document.createElement("button");
    let open=document.createElement("input");

    expand.innerHTML="&#8964;";
    expand.addEventListener("click",function(){
        let element=document.querySelector("#"+character.name.normalizeName()+"-table");
        if(element.classList.contains("hidden")){
            element.classList.remove("hidden");
        }else{
            element.classList.add("hidden");
        }
    });

    open.type="button";
    open.addEventListener("click",function(){
        let element=document.querySelector("#"+character.name.normalizeName());
        localStorage.setItem("character",element.value);
        window.location="character.html";
    });

    expand.classList.add("expand-option");

    div.appendChild(expand);
    div.appendChild(open);

    return div;
}

function LoadCharacterStats(character){
    let html=
        '<tbody>'+
            '<tr>'+
                '<th>Name</th>'+
                '<td>'+
                    '<input id="'+character.name+'-name" value='+character.name+' disabled/>'+
                '</td>'+
            '</tr>'+

            '<tr>'+
                '<th>Level</th>'+
                '<td>'+
                    '<input id="'+character.name+'-level" type="number" min=1 value='+character.level+' disabled/>'+
                '</td>'+
            '</tr>'+

            '<tr>'+
                '<th>Proficiency</th>'+
                '<td>'+
                    '<input id="'+character.name+'-proficiency" type="number" min=2 value='+Proficiency(character)+' disabled/>'+
                '</td>'+
            '</tr>'+
            '<tr>'+
                '<th>Hitpoints</th>'+
                '<td>'+
                    '<input type="number" class="hp" min=1 value='+character.hitpoints+' id="'+character.name+'-hitpoints"/>'+
                    '/'+
                    '<input type="number" class="hp" min=1 value='+character["max-hitpoints"]+' id="'+character.name+'-max-hitpoints"/>'+
                '</td>'+
            '</tr>'+
            '<tr>'+
                '<th>Armor Class</th>'+
                '<td>'+
                    '<input type="number" value='+character.ac+' id="'+character.name+'-ac"/>'+
                '</td>'+
            '</tr>'+
            '<tr>'+
                '<th>Initiative</th>'+
                '<td>'+
                    '<input type="number" value='+Modifier(character.dex)+' id="'+character.name+'-initiative"/>'+
                '</td>'+
            '</tr>'+
            '<tr>'+
                '<th>Passive Perception</th>'+
                    '<td>'+
                        '<input type="number" value='+PassivePerception(character)+' id="'+character.name+'-passperception"/>'+
                    '</td>'+
                '</tr>'+
            '<tr>'+
                '<th>Hit Dice</th>'+
                '<td>'+
                    '<input id="'+character.name+'-hit-dice" type="number" min=4 placeholder="6"/>'+
                '</td>'+
            '</tr>'+
            '<tr>'+
            '</tr>'+
        '</tbody>';
    let result=document.createElement("table");
    result.id=character.name.normalizeName().replaceAll(" ","-")+"-table";
    result.innerHTML=html;

    return result;
}