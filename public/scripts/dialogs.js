let dialogs={
    "character-export":{
        content:
        '<div class="modal-body">'+
            '<span class="close">&times;</span>'+
            '<h3>'+
                'If you wish to share your character sheet, you must send this chunk of text to the recipient.'+
            '</h3>'+
            '<textarea id="character-sheet-export">'+
            '</textarea>'+
        '</div>',
        postrender:function(){
            let elements=document.querySelectorAll(".close");
            for(let i=0;i<elements.length;i+=1){
                elements[i].addEventListener("click",HideModals);
            }

            window.addEventListener("click",function(event) {
                if (event.target.className==="modal"){
                    HideModals();
                }
            });
        }
    },

    "character-import":{
        content:
        '<div class="modal-body">'+
            '<span class="close">&times;</span>'+
            '<h3>'+
                'Please paste character data here.'+
            '</h3>'+
            '<textarea id="character-sheet-import">'+
            '</textarea>'+
            '<button id="import-data">Import</button>'+
        '</div>',
        postrender:function(){
            let elements=document.querySelectorAll(".close");
            for(let i=0;i<elements.length;i+=1){
                elements[i].addEventListener("click",HideModals);
            }

            window.addEventListener("click",function(event) {
                if (event.target.className==="modal"){
                    HideModals();
                }
            });
            
            document.getElementById("import-data").addEventListener("click",function(){
                importUserData(document.getElementById("character-sheet-import").value);
            });
        }
    },

    "character-load":{
        content:
        '<div class="modal-body">'+
            '<span class="close">&times;</span>'+
            '<h3>'+
                'Please select a character.'+
            '</h3>'+
            '<table id="character-table"></table>'+
            '<button id="load-data">Load</button>'+
        '</div>',
        postrender:function(){
            let elements=document.querySelectorAll(".close");
            for(let i=0;i<elements.length;i+=1){
                elements[i].addEventListener("click",HideModals);
            }

            window.addEventListener("click",function(event) {
                if (event.target.className==="modal"){
                    HideModals();
                }
            });
        }
    },

    "avatar-dialog":{
    content:
        '<div class="modal-body">'+
            '<span class="close">&times;</span>'+
            '<h3>'+
                'Please select an avatar.'+
            '</h3>'+
            
            '<div>'+
                '<label>From File system: </label>'+
                '<input type="file" id="avatar-fs"/>'+
            '</div>'+
            '<div>'+
                '<label>From Internet: </label>'+
                '<input type="text" id="avatar-browser"/>'+
            '</div>'+
                
            '<button id="load-avatar">Load</button>'+
        '</div>',
    postrender:function(){
        let elements=document.querySelectorAll(".close");
        for(let i=0;i<elements.length;i+=1){
            elements[i].addEventListener("click",HideModals);
        }

        window.addEventListener("click",function(event) {
            if (event.target.className==="modal"){
                HideModals();
            }
        });
        
        document.getElementById("load-avatar").addEventListener("click",function(){
            let fromBrowser=document.getElementById("avatar-browser");
            let fromFS=document.getElementById("avatar-fs");
            let image=document.getElementById("avatar");

            if(fromBrowser.value!==""){
                image.src=fromBrowser.value;
            }else{
                let reader  = new FileReader();

                reader.addEventListener("load", function () {
                    image.src = reader.result;
                }, false);

                if (fromFS.files[0]) {
                    reader.readAsDataURL(fromFS.files[0]);
                }
            }
         });
        }
    },

    "connect-lobby":{
        content:'<div class="modal-body">'+
        '<span class="close">&times;</span>'+
        '<h3>'+
            'Please select an avatar.'+
        '</h3>'+
        
        '<div>'+
            '<label>Token Of Host: </label>'+
            '<input type="text" id="token"/>'+
        '</div>'+
        '<input type="button" id="connect" value="Connect"/>',
        postrender:function(){
            document.querySelector("#connect").addEventListener("click",function(event){
                let token=document.querySelector("#token").value;
                let data=jwt_decode(token);
                localStorage.setItem("host",data.address);
            });
        }
    }
};