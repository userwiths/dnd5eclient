function CharacterFromDocument(){
	let characterSheet={};
	let other=document.querySelectorAll('.saveable');
	let items=document.querySelectorAll('.item-url');
	let spells=document.querySelectorAll('.magic-url');
	let languages=document.querySelectorAll('.language-url');

	let classes=document.querySelector('#class').selectedOptions;
	let subclasses=document.querySelector('#subclass').selectedOptions;

	characterSheet.avatar=document.querySelector('#avatar-canvas').toDataURL();
	characterSheet.level=document.querySelector('#level').value;
	characterSheet.name=document.querySelector('#name').value;
	characterSheet.race=document.querySelector('#race').value;
	
	characterSheet.class=[];
	for(let i=0;i<classes.length;i+=1){
		characterSheet.class.push(classes[i].value);
	}

	characterSheet.subclass=[];
	for(let i=0;i<subclasses.length;i+=1){
		characterSheet.subclass.push(subclasses[i].value);
	}
	
	for(let i=0;i<other.length;i+=1){
		if(other[i].type==='checkbox'){
			characterSheet[other[i].id]=other[i].checked;
		}else{
			characterSheet[other[i].id]=other[i].value;
		}
	}

	characterSheet.items=[];
	for(let i=0;i<items.length;i+=1){
		characterSheet.items.push(items[i].value);
	}
	
	characterSheet.spells=[];
	for(let i=0;i<spells.length;i+=1){
		characterSheet.spells.push(spells[i].value);
	}

	characterSheet.languages=[];
	for(let i=0;i<languages.length;i+=1){
		characterSheet.languages.push(languages[i].value);
    }
    
    if(document.querySelector('#index')!==null){
        characterSheet.index=document.querySelector('#index');
    }

	return characterSheet;
}
function ArmorFromDocument(){
    let result={};
    result.armor_class={};

    result.name=document.querySelector('#name').value;
    result.equipment_category=document.querySelector('#equipment_category').value;
    result.armor_category=document.querySelector('#armor_category').value;
    result.str_minimum=document.querySelector('#str_minimum').value;
    result.stealth_disadvantage=document.querySelector('#stealth_disadvantage').checked;
    result.weight=document.querySelector('#weight').value;

    result.armor_class.base=document.querySelector('#base').value;
    result.armor_class.dex_bonus=document.querySelector('#dex_bonus').checked;
    result.armor_class.max_bonus=document.querySelector('#max_bonus').value;

    result.cost=CostFromDocument();

    if(document.querySelector('#index')!==null){
        result.index=document.querySelector('#index');
    }

    return result;
}
function ItemFromDocument(){
    let result={};

    result.name=document.querySelector('#name').value;
    result.equipment_category=document.querySelector('#equipment_category').value;
    result.weight=document.querySelector('#weight').value;
    result.desc=document.querySelector('#desc').value;

    result.cost=CostFromDocument();

    if(document.querySelector('#index')!==null){
        result.index=document.querySelector('#index');
    }

    return result;
}
function WeaponFromDocument(){
    let result={};

    result.name=document.querySelector('#name').value;
    result.equipment_category=document.querySelector('#equipment_category').value;
    result.weapon_category=document.querySelector('#weapon_category').value;
    result.weapon_range=document.querySelector('#weapon_range').value;
    
    result.range=RangeFromDocument();
    result.throw_range=ThrowRangeFromDocument();

    result.weight=document.querySelector('#weight').value;

    if(document.querySelector('#index')!==null){
        result.index=document.querySelector('#index');
    }
}
function DamageTypeFromDocument(){

}

function ItemToDocument(item){
    let result=document.createElement('div');

    result.innerHTML='<h2>'+item.name+'</h2><textarea>'+item.desc+'</textarea>';
    result.appendChild(CostToDocument(item.cost));

    return result;
}

function RangeFromDocument(){
    let result={};

    result.normal=document.querySelector('#range-normal');
    result.long=document.querySelector('#range-long');

    return result;
}
function ThrowRangeFromDocument(){
    let result={};

    result.normal=document.querySelector('#throw-range-normal');
    result.long=document.querySelector('#throw-range-long');

    return result;
}

function CostToDocument(cost){
    let result=document.createElement('div');
    
    result.innerHTML='<label>Cost: '+cost.quantity+' '+cost.unit+'</label>';

    return result;
}
function CostFromDocument(){
    let cost={
        quantity:0,
        unit:'gold'
    };
    cost.quantity=document.querySelector('#quantity').value;
    cost.unit=document.querySelector('#unit').value;

    return cost;
}