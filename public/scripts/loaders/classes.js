function LoadClasses(urls){
	let items=document.querySelectorAll(".saving-proficiency");
	for(let e=0;e<items.length;e+=1){
		items[e].checked=false;
		items[e].disabled=false;
	}
	for(let i=0;i<urls.length;i+=1){
		LoadClassData(urls[i].value);
		if(urls[i].textContent.normalizeName()==="rogue"){
			LoadRogueExpertise();
		}
	}
}
function LoadClassData(url){
	let hitDice=document.querySelector("#hit-dice");
	
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		hitDice.value=entity.hit_die;
		for(let e=0;e<entity.saving_throws.length;e+=1){
			document.querySelector("#"+entity.saving_throws[e].name.normalizeName()+"-proficiency").checked=true;
			document.querySelector("#"+entity.saving_throws[e].name.normalizeName()+"-proficiency").disabled=true;
			CalculateStatusRelatedValues(entity.saving_throws[e].name.normalizeName());
		}
		LoadProficienciesSelection();
	}).fail((error)=>{
		console.log("An error occurred");
		console.log("URL: "+url);
		console.log("Error: "+error);
	});
}
function LoadRogueExpertise(){
	let elements=document.querySelectorAll(".skill-proficiency");

	let checkbox;

	for(let i=0;i<elements.length;i+=1){
		checkbox=document.createElement("input");
		checkbox.type="checkbox";
		checkbox.addEventListener("click",SkillExpertise);

		checkbox.id=elements[i].id.substring(0,elements[i].id.lastIndexOf("-"))+"-expertise";

		elements[i].parentElement.appendChild(checkbox);
	}
}

function LoadClassSpecificDataFor(level,url){
	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		let address=entity.class_levels.url;
		address=address.normalizeName();
		address=address.substring(0,address.lastIndexOf("s"));
		LoadClassSpecificData(address+"/"+level);
	}).fail(generalError);
}
function LoadClassSpecificData(url){
	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		if("spellcasting" in entity){
			let elem=document.querySelector("#magic-slots");
			let toRemove=document.querySelectorAll("#magic-slots .grid-body-item");
			let values=Object.values(entity.spellcasting);
			
			for(let i=0;i<toRemove.length;i+=1){
				toRemove[i].remove();
			}
			
			for(let i=0;i<values.length;i+=1){
				if(values[i]!==0){
					SpellSlotRow(i,entity.spellcasting.spells_known,values[i]);
				}
			}
		}
		if("sneak_attack" in entity.class_specific){

		}
	}).fail(generalError);
}

function LoadSubClasses(urls){
	for(let i=0;i<urls.length;i+=1){
		LoadSubClassData(urls[i]);
	}
}
function LoadSubClassData(url){
	let level=document.querySelector("#level").value;
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		if("spells" in entity){
			for(let i=0;i<entity.spells.length;i+=1){
				if("level_acquired" in entity.spells[i]){
					if(entity.spells[i].level_acquired<=level){
						LoadSpellData(entity.spells[i].spell.url);
					}
				}
			}
		}
		if("features" in entity){
			for(let i=0;i<entity.features.length;i+=1){
				LoadFeature(Infinity,entity.features[i].url);
			}
		}
	}).fail(generalError);
}