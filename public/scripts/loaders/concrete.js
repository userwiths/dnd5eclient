function LoadProficienciesSelection(){
	let box=document.querySelector("#proficiencies");
	let raceUrl=document.querySelector("#race").value;
	let option=document.createElement("option");
	
	return $.ajax({
		url: raceUrl,
		type: 'GET',
		dataType: 'json'
	}).done((entities)=>{
		for(let i=0;i<entities.starting_proficiencies.length;i+=1){
			option.textContent=entities.starting_proficiencies[i].name;
			option.value=entities.starting_proficiencies[i].url;
			box.appendChild(option);
			
			option=document.createElement("option");
		}
	}).fail(generalError);
}
function LoadProficienciesForClass(url){
	let option=document.createElement("option");
	let box=document.querySelector("#proficiencies");
	
	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entities)=>{
		for(let i=0;i<entities.proficiencies.length;i+=1){
			option.textContent=entities.proficiencies[i].name;
			option.value=entities.proficiencies[i].url;
			box.appendChild(option);
			
			option=document.createElement("option");
		}
	}).fail(generalError);
}

function LoadRaceData(url){
	let speed=document.querySelector("#speed");
	let size=document.querySelector("#size");
	
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		speed.value=entity.speed;
		size.value=entity.size;
		LoadProficienciesSelection();
	}).fail(generalError);
}
function LoadSkill(url){
	let element=document.querySelector('#skills-container');
	let rowNumber=nextRow('#skills label');
	let label=document.createElement('label');
	let checkBox=document.createElement('input');
	
	checkBox.type='checkbox';
	
	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((skills)=>{
		checkBox.id=skills.name.normalizeName()+'-proficiency';
		
		checkBox.addEventListener('click',SkillProficiency);
		if(skills.name.normalizeName()==='perception'){
			checkBox.addEventListener('click',CalculatePassivePerception);
		}

		checkBox.classList.add('skill-proficiency');
		checkBox.classList.add('saveable');
		checkBox.classList.add('grid-body-item');
		checkBox.classList.add(rowNumber);

		label.textContent=skills.name;
		label.classList.add('grid-body-item');
		label.classList.add(rowNumber);
		element.appendChild(label);

		label=document.createElement('input');
		label.type='number';
		label.disabled=true;
		label.id=skills.ability_score.name.normalizeName()+'-'+skills.name.normalizeName();
		label.classList.add('stat-skill');
		label.classList.add('rollable');
		label.value=document.querySelector('#'+skills.ability_score.name.normalizeName()+'-mod').value;

		element.appendChild(label);

		element.appendChild(checkBox);
		label=getRollButton(skills.ability_score.name.normalizeName()+'-mod',
					[skills.name.normalizeName()+'-proficiency',skills.name.normalizeName()+'-expertise']);
		element.appendChild(label);
		
	}).fail(generalError);
}
function LoadSpellData(url){
	let textArea=document.createElement("textarea");
	
	let urlHidden=document.createElement("input");
	urlHidden.type="hidden";
	urlHidden.className="magic-url";
	urlHidden.value=url;
	
	let label=document.createElement('label');
	let rowType=nextRow("#known-spells .inner-grid label");
	let lastRow=document.querySelector("#known-spells .inner-grid");
	
	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		label.textContent=entity.name;

		lastRow.appendChild(label);
		label.classList.add('grid-body-item');
		label.classList.add(rowType);
		label=document.createElement('label');

		if(entity.level===0){
			label.textContent="Cantrip";
		}else{
			label.textContent=entity.level;
		}
		lastRow.appendChild(label);
		label.classList.add('grid-body-item');
		label.classList.add(rowType);
		label=document.createElement('label');

		label.textContent=entity.casting_time;
		lastRow.appendChild(label);
		label.classList.add('grid-body-item');
		label.classList.add(rowType);
		label=document.createElement('label');

		label.textContent=entity.range;
		lastRow.appendChild(label);
		label.classList.add('grid-body-item');
		label.classList.add(rowType);
		label=document.createElement('label');

		label.textContent=entity.components.join(',');
		lastRow.appendChild(label);
		label.classList.add('grid-body-item');
		label.classList.add(rowType);
		label=document.createElement('label');

		label.textContent=entity.duration;
		lastRow.appendChild(label);
		label.classList.add('grid-body-item');
		label.classList.add(rowType);

		textArea.value=entity.desc.join("\n\t");
		lastRow.appendChild(textArea);
		lastRow.appendChild(urlHidden);
	}).fail(generalError);
}
function SpellSlotRow(level,known,slots){
	let element=document.querySelector('#magic-slots');
	let rowType=nextRow('#magic-slots label');
	let generateAttuneSelect=function(){
		let spells=knownSpellsFrom(-1);
		let select=document.createElement("select");
		let option=document.createElement("option");

		select.classList.add("attunement-slot");

		for(let i=0;i<spells.length;i+=1){
			option.value=spells[i].url;
			option.textContent=spells[i].name;

			select.appendChild(option);
			option=document.createElement("option");
		}
		select.addEventListener("change",function(event){
			let row=event.currentTarget.parentElement.parentElement;
			let list=row.lastChild.firstChild;
	
			let item=document.createElement("li");
			item.textContent=event.currentTarget.selectedOptions[0].textContent;
			list.appendChild(item);
		});
		return select;
	};

	let list=document.createElement("ul");
	list.classList.add("scrollable");

	let label=document.createElement("label");

	if(level===0){
		label.textContent="Cantrip";	
	}else{
		label.textContent=level;
	}

	label.classList.add('grid-body-item');
	label.classList.add(rowType);
	element.appendChild(label);
	label=document.createElement("label");

	label.classList.add('grid-body-item');
	label.classList.add(rowType);
	label.textContent=known;
	element.appendChild(label);
	label=document.createElement("label");

	label.classList.add('grid-body-item');
	label.classList.add(rowType);
	label.textContent=slots;
	element.appendChild(label);

	label=generateAttuneSelect();
	label.classList.add('grid-body-item');
	label.classList.add(rowType);
	element.appendChild(label);

	label=document.createElement('div');
	label.classList.add('grid-body-item');
	label.classList.add(rowType);
	label.id='spell-slot-'+level;
	element.appendChild(label);
}
function UseSpell(){}

function LoadAbility(url){
	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((ability)=>{
		let row=document.createElement("tr");
		let cell= document.createElement("td");
		let temp=document.createElement("input");

		cell.textContent=ability.full_name;
		row.appendChild(cell);
		cell= document.createElement("td");

		temp.type="number";
		temp.disabled=true;
		temp.id=ability.name.normalizeName()+"-mod";
		temp.classList.add("rollable");
		row.appendChild(WrapIn("td",temp));

		temp=document.createElement("input");
		temp.type="number";
		temp.id=ability.name.normalizeName();
		temp.classList.add("status-value");
		temp.classList.add("saveable");
		row.appendChild(WrapIn("td",temp));

		temp=document.createElement("input");
		temp.type="checkbox";
		temp.id=ability.name.normalizeName();
		temp.classList.add("saving-proficiency");
		temp.classList.add("saveable");
		row.appendChild(WrapIn("td",temp));
		
	}).fail(generalError);
}

function LoadFeaturesFor(level,charClass){
	return $.ajax({
		url: 'http://www.dnd5eapi.co/api/classes/'+charClass.normalizeName()+'/level/'+level,
		type: 'GET',
		dataType: 'json'
	}).done((data)=>{
		for(let i=0;i<data.features.length;i+=1){
			LoadFeature(level,data.features[i].url);
		}
	}).fail(generalError);
}
function LoadFeature(level,url){
	let elem=document.querySelector('#features-container');
	let rowType=nextRow('#features-container label');
	let label=document.createElement('label');
	let textArea=document.createElement('textarea');

	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((feature)=>{
		if(feature.level>level){
			return;
		}

		label.textContent=feature.name;
		label.classList.add('grid-body-item');
		label.classList.add(rowType);
		elem.appendChild(label);

		textArea.value=feature.desc;
		elem.appendChild(textArea);
	}).fail(generalError);
}

function LoadLanguages(urls){
	for(let i=0;i<urls.length;i+=1){
		LoadLanguage(urls[i]);
	}
}
function LoadLanguage(url){
	let rowNumber=nextRow('#language-container label');
	let elem=document.querySelector('#language-container');
	let label=document.createElement('label');
	let hidden=document.createElement('input');

	hidden.type='hidden';
	hidden.classList.add('language-url');
	hidden.value=url;
	hidden.classList.add(rowNumber);

	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((language)=>{
		label.textContent=language.name;
		label.classList.add('grid-body-item');
		label.classList.add(rowNumber);
		elem.appendChild(label);
		label=document.createElement('label');
	
		label.textContent=language.typical_speakers.join(", ");
		label.classList.add('grid-body-item');
		label.classList.add(rowNumber);
		elem.appendChild(label);
		label=document.createElement('label');

		label.textContent=language.script;
		label.classList.add('grid-body-item');
		label.classList.add(rowNumber);
		elem.appendChild(label);
		label=document.createElement('label');

		label.textContent=language.type;
		label.classList.add('grid-body-item');
		label.classList.add(rowNumber);
		elem.appendChild(label);
		label=document.createElement('label');

		elem.appendChild(hidden);
	}).fail(generalError);
}

function addRolls(){	
	let elements=document.querySelectorAll(".rollable");
	for(let i=0;i<elements.length;i+=1){
		if(elements[i].id.includes("-mod")){
			addRoll(elements[i],
				elements[i].id,
				[]);
		}else if(elements[i].id.includes("saving")){
			addRoll(elements[i],
					elements[i].id,
					[]);
		}else{
			addRoll(elements[i],
				elements[i].id,
				[elements[i].id.substring(0,elements[i].id.indexOf("-"))+"-proficiency",
			elements[i].id.substring(0,elements[i].id.indexOf("-"))+"-expertise"]);
		}
	}
}
function addRoll(element,mod,prof){
	element.parentNode.appendChild(getRollButton(mod,prof));
}
function getRollButton(mod,prof){
	let button=document.createElement("button");
	
	button.textContent="Roll";
		
	button.addEventListener("click",function(){
		RollEvent({details:{
			modifierId:mod,
			proficienciesIds:prof
		}});
	});
	button.addEventListener("roll",RollEvent);

	return button;
}

function GenerateRequired(){
	addRolls();
}
function LoadRequired(){
	let loadCustom=(url_addition,singleLoader)=>{
		let url=localStorage.getItem("host");

		if(url===null){
			return;
		}

		url=url+url_addition;

		return singleLoader(url);
	};

	LoadClassSelection().done(()=>SelectionRemoteLoader(settings.classes_url,"#class")).fail();//Classes are always first
	LoadSkills().done(()=>loadCustom(settings.skills_url,LoadSkill)).fail();
	LoadRaceSelection().done(()=>SelectionRemoteLoader(settings.races_url,"#race")).fail();
	LoadSpellSelection().done(()=>SelectionRemoteLoader(settings.spells_url,"#spell")).fail();
	LoadItemSelection().done(()=>SelectionRemoteLoader(settings.items_url,'#item-select')).fail();
	LoadArmorSelection().done(()=>SelectionRemoteLoader(settings.armors_url,'#armor-select')).fail();
	LoadWeaponSelection().done(()=>SelectionRemoteLoader(settings.weapons_url,'#weapon-select')).fail();
	LoadLanguagesSelect().done(()=>SelectionRemoteLoader(settings.languages_url,"#language")).fail();
}