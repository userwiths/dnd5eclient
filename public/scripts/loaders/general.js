function SelectionRemoteLoader(url_addition,selector){
	let box=document.querySelector(selector);
	let option=document.createElement("option");
	let url=localStorage.getItem("host");
		
	if(url===null){
		return;
	}

	url=url+'/';

	return $.ajax({
		url: url+url_addition,
		type: 'GET',
		dataType: 'json'
	}).done((entities)=>{
		for(let i=0;i<entities.results.length;i+=1){
			option.textContent=entities.results[i].name;
			option.value=url+entities.results[i].url;
			box.appendChild(option);
				
			option=document.createElement("option");
		}
	}).fail(generalError);
}
function SelectionLoader(url,selector,property='results'){
	let box=document.querySelector(selector);
	let option=document.createElement("option");

	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entities)=>{
		for(let i=0;i<entities[property].length;i+=1){
			option.textContent=entities[property][i].name;
			option.value=entities[property][i].url;
			box.appendChild(option);
				
			option=document.createElement("option");
		}
	}).fail(generalError);
}

function LoadSkills(){
	let toRemove=document.querySelectorAll('#skills-container .grid-body-item');

	for(let i=0;i<toRemove.length;i+=1){
		toRemove[i].remove();
	}

	return $.ajax({
		url: settings.host+settings.skills_url,
		type: 'GET',
		dataType: 'json'
	}).done((skills)=>{
		for(let i=0;i<skills.results.length;i+=1){
			LoadSkill(skills.results[i].url);
		}
	}).fail(generalError);
}
function LoadRaceSelection(){
	RemoveChildrenOf('#race');
	return SelectionLoader(settings.host+settings.races_url,'#race');
}
function LoadFeatures(){
	RemoveChildrenOf("#feature-table");

	let classes=document.querySelector("#class").selectedOptions;
	let level=document.querySelector("#level").value;

	if(level<=0){
		return;
	}

	for(let e=0;e<classes.length;e+=1){
		for(let i=1;i<=level;i+=1){
			LoadFeaturesFor(i,classes[e].text.normalizeName());
		}
	}
}
function LoadClassSpecific(){
	let urls=document.querySelector("#class").selectedOptions;
	let level;
	for(let i=0;i<urls.length;i+=1){
		level=document.querySelector("#"+urls[i].text.normalizeName()+"-level").value;
		LoadClassSpecificDataFor(level,urls[i].value);
	}
}
function LoadClassSelection(){	
	RemoveChildrenOf("#class");
	return SelectionLoader(settings.host+settings.classes_url,'#class');
}

function LoadSubClassSelection(){
	RemoveChildrenOf("#subclass");

	return $.ajax({
		url: settings.host+settings.subclasses_url,
		type: 'GET',
		dataType: 'json'
	}).done((entities)=>{
		for(let i=0;i<entities.results.length;i+=1){
			CheckSubClassValidity(entities.results[i].url);
		}
	}).fail(generalError);
}
function LoadSubClassSelectionFor(className){
	RemoveChildrenOf("#subclass");

	return $.ajax({
		url: settings.host+settings.subclasses_url,
		type: 'GET',
		dataType: 'json'
	}).done((entities)=>{
		for(let i=0;i<entities.results.length;i+=1){
			CheckSubClassValidityFor(entities.results[i].url,className);
		}
	}).fail(generalError);
}
function CheckSubClassValidityFor(url,className){
	let box=document.querySelector("#subclass");
	let option=document.createElement("option");
	
	return $.ajax({
		url:url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		if(entity.class.name.normalizeName()===className){
			option.textContent=entity.name;
			option.value=entity.url;
			box.appendChild(option);
				
			option=document.createElement("option");
		}
	}).fail(generalError);
}
function CheckSubClassValidity(url){
	let box=document.querySelector("#subclass");
	let option=document.createElement("option");
	
	return $.ajax({
		url:url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		if(SelectedContainsValue(entity.class.url,"#class")){
			option.textContent=entity.name;
			option.value=entity.url;
			box.appendChild(option);
				
			option=document.createElement("option");
		}
	}).fail(generalError);
}

function LoadSpellSelection(){
	RemoveChildrenOf('#magic-select');
	return SelectionLoader(settings.host+settings.spells_url,'#magic-select');
}

function LoadWeaponSelection(){	
	RemoveChildrenOf("#weapon-select");
	return SelectionLoader(settings.host+settings.weapons_url,'#weapon-select','equipment');
}
function LoadArmorSelection(){
	RemoveChildrenOf("#armor-select");
	return SelectionLoader(settings.host+settings.armors_url,'#armor-select','equipment');
}
function LoadItemSelection(){
	RemoveChildrenOf("#item-select");
	return SelectionLoader(settings.host+settings.items_url,'#item-select','equipment');
}

function LoadAbilities(){
	let elements=document.querySelector("#");
	let ability;
	$.ajax({
		url: settings.host+settings.ability_url,
		type: 'GET',
		dataType: 'json'
	}).done((abilities)=>{
		for(let i=0;i<abilities.results.length;i+=1){
			ability=LoadAbility(abilities.results[i].url);
			elements.appendChild(ability);
		}
	}).fail(generalError);
}
function LoadLanguagesSelect(){
	let selection=document.querySelector("#language");
	let option=document.createElement("option");
	
	option.value=0;
	selection.innerHTML="";
	selection.appendChild(option);
	option=document.createElement("option");
	
	return $.ajax({
		url: settings.host+settings.languages_url,
		type: 'GET',
		dataType: 'json'
	}).done((entities)=>{
		for(let i=0;i<entities.results.length;i+=1){
			option.textContent=entities.results[i].name;
			option.value=entities.results[i].url;
			selection.appendChild(option);
			
			option=document.createElement("option");
		}
	}).fail(generalError);	
}
function LoadProficienciesForClasses(){
	RemoveChildrenOf("#proficiencies");
	
	let elem=document.querySelector("#class").selectedOptions;
	for(let i=0;i<elem.length;i+=1){
		LoadProficienciesForClass(elem[i].value);
	}
}