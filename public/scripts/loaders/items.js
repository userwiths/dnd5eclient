function LoadWeaponData(url){	
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		LoadWeaponFromData(entity);
	}).fail(generalError);
}
function LoadWeaponFromData(entity){
	let lastRow=document.querySelector('#weapon-container');
	let evenRow=nextRow('#weapon-container label');
	let name=lastRow.appendChild(document.createElement('label'));
	let dice=lastRow.appendChild(document.createElement('label'));
	let damageType=lastRow.appendChild(document.createElement('label'));

	let urlHidden=document.createElement('input');
	urlHidden.type='hidden';
	urlHidden.classList.add('item-url');
	urlHidden.classList.add(evenRow);
	urlHidden.value=entity.url;

	name.textContent=entity.name;
	name.classList.add('grid-body-item');
	name.classList.add(evenRow);
	dice.textContent=entity.damage.dice_count+'d'+entity.damage.dice_value;
	dice.classList.add('grid-body-item');
	dice.classList.add(evenRow);
	damageType.textContent=entity.damage.damage_type.name;
	damageType.classList.add('grid-body-item');
	damageType.classList.add(evenRow);

	lastRow.appendChild(name);
	lastRow.appendChild(dice);
	lastRow.appendChild(damageType);
	lastRow.appendChild(urlHidden);
}

function LoadEquipmentData(url){
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		LoadEquipmentFromData(entity);
	}).fail(generalError);
}
function LoadEquipmentFromData(entity){
	let lastRow=document.querySelector("#items-container");
	let evenRow=nextRow('#items-container label');
	let name=document.createElement('label');
	let weight=document.createElement('label');
	let category=document.createElement('label');
	
	let urlHidden=document.createElement("input");
	urlHidden.type="hidden";
	urlHidden.className="item-url";
	urlHidden.value=entity.url;
	urlHidden.classList.add(evenRow);

	name.textContent=entity.name;
	name.classList.add('grid-body-item');
	name.classList.add(evenRow);
	weight.textContent=entity.weight;
	weight.classList.add('grid-body-item');
	weight.classList.add(evenRow);
	category.textContent=entity.equipment_category;
	category.classList.add('grid-body-item');
	category.classList.add(evenRow);

	lastRow.appendChild(name);
	lastRow.appendChild(weight);
	lastRow.appendChild(category);
	lastRow.appendChild(urlHidden);
}

function LoadArmorData(url){
	$.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		LoadArmorFromData(entity);
	}).fail(generalError);
}
function LoadArmorFromData(entity){
	let table=document.querySelectorAll("#armor-container label.grid-body-item");
	
	let urlHidden=document.createElement("input");
	urlHidden.type="hidden";
	urlHidden.className="item-url";
	urlHidden.value=entity.url;
	
	table[0].textContent=entity.name;
	table[1].textContent=entity.armor_category;
	table[2].textContent=entity.armor_class.base;
	table[3].textContent=entity.stealth_disadvantage;
	table[4].textContent=entity.str_minimum;
	table[5].textContent=entity.armor_class.dex_bonus;
	table[6].textContent=entity.armor_class.max_bonus;
	table[7].textContent=entity.weight;
		
	document.querySelector("#armor-container").appendChild(urlHidden);

	let dex=document.querySelector("#dex-mod").value;
	let ac=document.querySelector("#ac");

	ac.value=parseInt(entity.armor_class.base);
	if(entity.armor_class.dex_bonus===true){
		if(entity.armor_class.max_bonus===null || entity.armor_class.max_bonus>dex){
			ac.value=parseInt(ac.value)+parseInt(dex);
		}else{
			ac.value=parseInt(ac.value)+parseInt(entity.armor_class.max_bonus);
		}
	}
}

function LoadItemData(url){	
	return $.ajax({
		url: url,
		type: 'GET',
		dataType: 'json'
	}).done((entity)=>{
		if(entity.equipment_category==="Weapon"){
			LoadWeaponFromData(entity);
		}else if(entity.equipment_category==="Armor"){
			LoadArmorFromData(entity);
		}else{
			LoadEquipmentFromData(entity);
		}
	}).fail(generalError);
}