let settings={
    host:'http://www.dnd5eapi.co/api/',
    skills_url:'skills',
    races_url:'races',
    classes_url:'classes',
    subclasses_url:'subclasses',
    spells_url:'spells',
    items_url:'equipment-categories/3',//'equipment',
    ability_url:'ability-scores',
    languages_url:'languages',
    weapons_url:'equipment-categories/1',
    armors_url:'equipment-categories/2',

    character_dependant:["#weapon-table","#spell-table","#equipment-table","#language-table","#feature-table"],
    disabled:true,
    save_host:''
};

let mapper={
    ability:{
        any:{
            modifier:"*-mod",
            saving:{
                has_saving:true,
                saving:"*-saving",
                proficiency:"*-proficiency"
            },
            proficiency:{
                has_proficiency:false
            },
            skills:{
                template:"*-?",
                proficiency:"?-proficiency"
            }
        }
    }
};

let notifier_options={
    "closeButton": true,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": true,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
};