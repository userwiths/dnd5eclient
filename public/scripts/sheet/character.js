function exportUserData(){
	let characterSheet=CharacterFromDocument();
	LoadDialog('character-export');

	//Hide it as to not be seen by user.
	document.querySelector('#modal').classList.add('hidden');

	document.querySelector('#character-sheet-export').value=JSON.stringify(characterSheet);
	localStorage.setItem('character',JSON.stringify(characterSheet));
}
function importFromFile(event){
  let input = event.currentTarget;
  let reader = new FileReader();
  reader.onload = function(){
    let text = reader.result;
    importUserData(text);
  };
  reader.readAsText(input.files[0]);
}
function importUserData(data){
	let characterSheet=JSON.parse(data);
	let obj=Object.keys(characterSheet);
	let ev=new Event('change');
	
	let element;
	
	ClearDependant();
	
	for(let i=0;i<obj.length;i+=1){
		if(obj[i]==='items'  ||
			obj[i]==='spells' ||
			obj[i]==='class' ||
			obj[i]==='subclass' || 
			obj[i]==='languages' || 
			obj[i]==='avatar' ){
			continue;
		}

		if(obj[i].endsWith('-level')){
			AddLevelFor(obj[i].split('-')[0],LevelUpEvent);
		}

		element=document.querySelector('#'+obj[i]);
		if(element===null){
			console.log('Could not find : '+obj[i]);
			continue;
		}
		if(element.type==='checkbox'){
			element.checked=characterSheet[obj[i]];
		}else{
			element.value=characterSheet[obj[i]];
		}
		element.dispatchEvent(ev);
	}
	
	if('class' in characterSheet){
		element=document.querySelector('#class');
		for(let i=0;i<characterSheet.class.length;i+=1){
			for(let e=0;e<element.childElementCount;e+=1){
				if(element[e].value===characterSheet.class[i]){
					element[e].selected=true;
				}
			}
			element.dispatchEvent(ev);
		}
	}	

	if('subclass' in characterSheet){
		element=document.querySelector('#subclass');
		for(let i=0;i<characterSheet.subclass.length;i+=1){
			for(let e=0;e<element.childElementCount;e+=1){
				if(element[e].value===characterSheet.subclass[i]){
					element[e].selected=true;
				}
			}
			element.dispatchEvent(ev);
		}
	}

	if('items' in characterSheet){
		for(let i=0;i<characterSheet.items.length;i+=1){
			LoadItemData(characterSheet.items[i]);
		}
	}
	
	if('spells' in characterSheet){
		for(let i=0;i<characterSheet.spells.length;i+=1){
			LoadSpellData(characterSheet.spells[i]);
		}
	}

	if('languages' in characterSheet){
		for(let i=0;i<characterSheet.languages.length;i+=1){
			LoadLanguage(characterSheet.languages[i]);
		}
	}

	document.querySelector('#avatar').src=characterSheet.avatar;
}
function downloadCharacter(){
	exportUserData();

	let exportModel=document.querySelector('#character-sheet-export');
	DownloadData(exportModel.value);
}

function Proficiency(character){
	return Math.ceil(1+(character.level/4));
}
function PassivePerception(character){
	let result=10+parseInt(Modifier(character.wis));
	if(character['perception-proficiency']){
		result+=Proficiency(character);
	}
	return result;
}
function Initiative(character){
	return character['dex-mod'];
}
function Modifier(value){
	return Math.floor((value-10)/2);
}
//Ability OR Stat
function IsProficient(name){
	let element;

	if(name.includes('-mod')){
		return false;
	}
	

	if(name.endsWith('-saving')){
		//Is stat.
		element=document.querySelectorAll('.saving-proficiency');
	}else{
		//Is Skill
		element=document.querySelectorAll('.skill-proficiency');
	}

	for(let i=0;i<element.length;i+=1){
		if(element[i].id.includes(name)){
			return element[i].checked;
		}
	}
	return false;
}
//The rogue class Expertise trait
function Expertise(name){
	let element;//=
}