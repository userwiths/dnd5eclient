function BindAll(){
	BindStatusHandlers();
	BindCollapseOnTitle();
	BindButtons();
	BindAvatarLoad();
	BindJQueryEventHandlers();
	BindClassChangeHandlers();
	BindModals();
}

function BindStatusHandlers(){
	let elements=document.querySelectorAll('.status-value');
	
	let calculateStatusGeneral=function(event){
		CalculateStatusRelatedValues(event.target.id);
	};
	let calculateStatusProficiency=function(event){
		CalculateStatusRelatedValues(event.target.id.split('-')[0]);
	};

	for(let i=0;i<elements.length;i+=1){
		elements[i].min=1;
		elements[i].max=30;

		elements[i].addEventListener('change',calculateStatusGeneral);
		if(elements[i].id==='dex'){
			elements[i].addEventListener('change',CalculateAC);
		}
		document.querySelector('#'+elements[i].id+'-proficiency').addEventListener('click',calculateStatusProficiency);
	}	
}
function BindAvatarLoad(){
	document.querySelector("#avatar").addEventListener("load",function(){
		resizeImage("#avatar","#avatar-canvas");
	});
}

function LevelUpEvent(event){
	let allLevels=document.querySelectorAll(".class-level");
	let generalLevel=document.querySelector("#level");
	let className=event.currentTarget.id.split("-")[0];
	let level=event.currentTarget.value;
	
	generalLevel.value=0;
	for(let i=0;i<allLevels.length;i+=1){
		generalLevel.value=parseInt(generalLevel.value)+parseInt(allLevels[i].value);
	}

	CalculateProficiency();

	LoadFeaturesFor(level,className);
	LoadClassSpecificDataFor(level,UrlForClass(className));
	if(level===3){
		LoadSubClassSelectionFor(className);
	}
}

function BindClassChangeHandlers(){
	let assertLevels=()=>{
		let classes=document.querySelector("#class").selectedOptions;
		let temp;
		
		for(let i=0;i<classes.length;i+=1){
			temp=document.querySelector("#"+classes[i].text.normalizeName()+"-level");
			if(temp===null){
				temp=GenerateLevelFor(classes[i].text.normalizeName());
				temp.addEventListener("change",LevelUpEvent);
				return temp;
			}
		}
		return null;
	};

	$("#class").on("change",function(e){
		let lastClass=e.currentTarget.selectedOptions[e.currentTarget.selectedOptions.length-1].text.normalizeName();
		let item=document.querySelector("#character-levels");
		let temp=assertLevels();

		if(temp!==null){
			item.appendChild(temp);
			document.querySelector("#level").value=parseInt(document.querySelector("#level").value)+1;
		}

		LoadClasses(e.currentTarget.selectedOptions);
		LoadSubClassSelection();

		LoadProficienciesForClasses();
		LoadClassSpecific();
		if(e.currentTarget.selectedOptions.length>0){
			LoadFeaturesFor(1,lastClass);
		}
	});
	$("#subclass").on("change",function(e){
		for(let i=0;i<e.currentTarget.selectedOptions.length;i+=1){
			LoadSubClassData(e.currentTarget.selectedOptions[i].value);
			LoadClassSpecific();
		}
	});
}
function BindJQueryEventHandlers(){
	$('select').select2({
		width:'150px'
	});

	$('#class').select2({
		width:'150px',
		multiple:true
	});
	$('#subclass').select2({
		width:'150px',
		multiple:true
	});

	$('select').on('change',function(e){
		let elements=document.querySelectorAll('.status-value');
		for(let i=0;i<elements.length;i+=1){
			CalculateStatusRelatedValues(elements[i].id);
		}
	});
	
	$('#race').on('change',function(e){
		LoadRaceData(e.currentTarget.value);
	});

	$('#item-select').on('change',function(e){
		LoadItemData(e.currentTarget.value);
	});
	$('#weapon-select').on('change',function(e){
		LoadItemData(e.currentTarget.value);
	});
	$('#armor-select').on('change',function(e){
		LoadItemData(e.currentTarget.value);
	});

	$('#magic-select').on('change',function(e){
		LoadSpellData(e.currentTarget.value);
	});
	$('#character-selector').on('change',function(e){
		importUserData(JSON.parse(localStorage.getItem('characters'))[e.currentTarget.value]);
	});
	$('#spell-casting-ability').on('change',function(e){
		e.currentTarget.disabled=true;
		let proficiency=document.querySelector('#proficiency');
		let mod=document.querySelector('#'+e.currentTarget.value+'-mod');
		let elem=document.querySelector('#spell-save-dc');
		let attack_mod=document.querySelector('#spell-attack-mod');
		
		elem.innerHTML=8+parseInt(mod.value);
		attack_mod.innerHTML=8+parseInt(mod.value)+parseInt(proficiency.value);
	});
	$("#language").on("change",function(e){
		LoadLanguage(e.currentTarget.value);
	});

	$(".saveable").on("click",function(e){
		toastr.success("There are unsaved changes. Save Now.");

		toastr.options = notifier_options;
		toastr.options.onclick=function(){
			downloadCharacter();
		};				
	});
}
function SkillProficiency(event){
	let prof=document.querySelector("#proficiency").value;
	let skillName=event.currentTarget.id.substring(0,event.currentTarget.id.lastIndexOf("-"));
	let statMod=getSkillStat(skillName);
	let elem=document.querySelector("#"+statMod+"-mod");
	let val=elem.value;

	if(event.target.checked){
		val=parseInt(val)+parseInt(prof);
	}

	document.querySelector("#"+statMod+"-"+skillName).value=val;
}
function SkillExpertise(event){
	let prof=document.querySelector("#proficiency").value;
	let skillName=event.currentTarget.id.substring(0,event.currentTarget.id.lastIndexOf("-"));
	let statMod=getSkillStat(skillName);
	let elem=document.querySelector("#"+statMod+"-mod");

	let proficient=IsProficient(skillName);
	let val=elem.value;

	if(proficient){
		val=parseInt(val)+parseInt(prof);
	}

	if(event.target.checked){
		val=parseInt(val)+parseInt(prof);
	}

	document.querySelector("#"+statMod+"-"+skillName).value=val;
}
function RollEvent(event){
	let result=document.createElement("div");
	let modElement=document.querySelector("#"+event.details.modifierId);
		
	let dice=roll(1,20);
	let proficiency=document.querySelector("#proficiency").value;
	let rollResult=0;

	result.innerHTML="Dice"+"	"+"Modifier </br>";
			
	result.innerHTML+=modElement.id.split("-")[0];
	if(modElement.id.endsWith("saving")){
		result.innerHTML+=" saving";
	}

	result.innerHTML+=" throw: "+dice;
	if(modElement.value>=0){
		result.innerHTML+="+";
	}

	rollResult+=parseInt(dice);
	rollResult+=parseInt(modElement.value);
	result.innerHTML+=modElement.value;
	for(let i=0;i<event.details.proficienciesIds.length;i+=1){
		if(document.querySelector("#"+event.details.proficienciesIds[i])!==null){
			if(document.querySelector("#"+event.details.proficienciesIds[i]).checked){
				result.innerHTML+=" +" +proficiency;
				rollResult+=parseInt(proficiency);
			}
		}
	}

	result.innerHTML+=" = "+rollResult;
	result.innerHTML+="</br></br>";

	document.querySelector("#player-action").appendChild(result);
}
function BindButtons(){
	document.querySelector("#character-file").addEventListener("change",importFromFile);
	
	document.querySelector("#save-as").addEventListener("click",function(){
		downloadCharacter();
	});

	document.querySelector("#save").addEventListener("click",function(){
		exportUserData();
		window.close();//location="characters.html";
	});
}
function BindCollapseOnTitle(){
	$('.hide-trigger').on('click',function(event){
		let parent=event.currentTarget.parentElement;
		let grandparent=parent.parentElement;

		for(let i=0;i<parent.childNodes.length;i+=1){
			if(parent.childNodes[i]!==event.currentTarget && parent.childNodes[i].nodeType!==Node.TEXT_NODE){
				parent.childNodes[i].classList.toggle('hidden');
			}
		}

		for(let i=0;i<grandparent.childNodes.length;i+=1){
			if(grandparent.childNodes[i]!==parent && grandparent.childNodes[i].nodeType!==Node.TEXT_NODE){
				grandparent.childNodes[i].classList.toggle('hidden');
			}
		}
	});
}
function BindModals(){
	document.querySelector("#load").addEventListener("click",function() {
		LoadDialog("character-load");
		getLocalCharacters();
	});
	document.querySelector("#export").addEventListener("click",function() {
		LoadDialog("character-export");
		exportUserData();
	});
	document.querySelector("#import").addEventListener("click",function() {
		LoadDialog("character-import");
	});
	document.querySelector("#set-avatar").addEventListener("click",function() {
		LoadDialog("avatar-dialog");
	});
}
