function CalculateStatusRelatedValues(ability){
	let stat=document.querySelector("#"+ability).value;
	let modifier=document.querySelector("#"+ability+"-mod");
	
	let value=Math.floor((stat-10)/2);
	
	modifier.value=value;
	
	CalculateSaving(ability);
	CalculateSkillsFor(ability);

	if(ability==="wis"){
		CalculatePassivePerception();
	}
	if(ability==="dex"){
		document.querySelector("#initiative").value=value;
	}
}
function CalculateSaving(ability){
	let abilityMap=MapAbility(ability);

	let proficiency=document.querySelector("#proficiency").value;

	let saving=document.querySelector("#"+abilityMap.saving.saving);
	let proficientElement=document.querySelector("#"+abilityMap.saving.proficiency.replace("*",ability));
	let modifier=document.querySelector("#"+abilityMap.modifier.replace("*",ability));
	
	saving.value=modifier.value;
	if(proficientElement.checked){
		saving.value=parseInt(saving.value)+parseInt(proficiency);
	}
}
function CalculateSkillsFor(ability){
	let proficiency=document.querySelector("#proficiency").value;
	let modifier=document.querySelector("#"+ability+"-mod").value;
	let elements=document.querySelectorAll(".stat-skill");
	let isProficient=false;
	let skillName;

	if(ability==="wis"){
		CalculatePassivePerception();
	}
	for(let i=0;i<elements.length;i+=1){
		if(elements[i].id.startsWith(ability)){
			skillName=elements[i].id.substring(elements[i].id.indexOf("-")+1);
			
			isProficient=document.querySelector("#"+skillName+"-proficiency").checked;
			elements[i].value=parseInt(modifier);
			if(isProficient){
				elements[i].value=parseInt(elements[i].value)+parseInt(proficiency);
			}
			elements[i].dispatchEvent(new Event("changed"));
		}
	}
}
function CalculateForSkill(skill){
	let statName=getSkillStat(skill);
	let proficiency=document.querySelectorAll("#proficiency").value;
	let modifier=document.querySelectorAll("#"+statName+"-mod").value;
	let isProficient=document.querySelectorAll("#"+skill+"-proficiency").checked;
	let skillElement=document.querySelectorAll("#"+statName+"-"+skill);
	
	skillElement.value=parseInt(modifier);
	if(isProficient){
		skillElement.value=parseInt(skillElement.value)+parseInt(proficiency);
	}
	skillElement.dispatchEvent(new Event("changed"));
}
function getSkillStat(skill){
	let elements=document.querySelectorAll(".stat-skill");
	for(let i=0;i<elements.length;i+=1){
		if(elements[i].id.endsWith(skill)){
			return elements[i].id.split("-")[0];
		}
	}
}
function CalculateProficiency(){
	let level=document.querySelector("#level").value;
	document.querySelector("#proficiency").value=Math.ceil(1+(level/4));
}
function CalculatePassivePerception(){
	let element=document.querySelector("#passperception");
	let wis=document.querySelector("#wis-mod").value;
	let proficient=IsProficient("perception");
	let proficiency=document.querySelector("#proficiency").value;

	element.value=10+parseInt(wis);
	if(proficient){
		element.value=parseInt(element.value)+parseInt(proficiency);
	}
}
function CalculateAC(){
	let element=document.querySelectorAll("#armor-container label.grid-body-item");
	let dex=document.querySelector("#dex-mod").value;
	let ac=document.querySelector("#ac");
	let result;

	result=parseInt(element[2].textContent);
	if(element[5].textContent==='true'){
		if(parseInt(element[6].textContent)===null || parseInt(element[6].textContent)>dex){
			result=parseInt(result)+parseInt(dex);
		}else{
			result=parseInt(result)+parseInt(element[6].textContent);
		}
	}
	ac.value=result;
}