String.prototype.replaceAll=function(old,nw){
	return this.split(old).join(nw);//replace(/ /g, "-");
};
String.prototype.normalizeName=function(){
	let result=this;
	
	if(this.includes(' ')){
		result = result.replaceAll(" ","-");
	}
	result = result.toLowerCase();
	
	return result;
};

function resizeImage(imgId,canvasId){
	let img=document.querySelector(imgId);
	let canvas=document.querySelector(canvasId);
	let context=canvas.getContext("2d");

	context.drawImage(img,0,0,200,150);
}
function nextRow(selector){
	let element=document.querySelectorAll(selector);

	if(element.length===0){
		return 'grid-even-row';
	}

	element=element[element.length-1];
	if(element.classList.contains('grid-even-row')){
		return 'grid-odd-row';
	}else{
		return 'grid-even-row';
	}
}

function roll(diceNumber,diceSides){
	if(diceSides===undefined){
		diceSides=20;
	}
	return diceNumber*(Math.round(Math.random()*(diceSides-1))+1);
}
function HideModals(){
	let elements=document.querySelectorAll(".modal");
	for(let i=0;i<elements.length;i+=1){
		elements[i].classList.add("hidden");
	}
}
function Hide(item){
	let childs=item.parentElement.childNodes;
	for(let e=2;e<childs.length;e+=1){
		if(childs[e].nodeName==="#text"){
			continue;
		}
		
		if(!item.parentElement.classList.contains("height-correct")){
			item.parentElement.classList.add("height-correct");
		}else{
			item.parentElement.classList.remove("height-correct");
		}

		if(!childs[e].classList.contains("hidden")){
			childs[e].classList.add("hidden");
			childs[e].classList.add("inline-element");
		}else{
			childs[e].classList.remove("hidden");
			childs[e].classList.remove("inline-element");
		}
	}
}
function generalError($xhr){
	console.error("An error occured. Response: "+this.url);
	console.error(this);
}

function RemoveChildrenOf(selector){
	let element;
	let elements=document.querySelectorAll(selector);
	for(let i=0;i<elements.length;i+=1){
		element=elements[i];
		while (element.childNodes.length > 0) {
			element.removeChild(element.lastChild);
		}
	}
}
function LeaveOnlyChildrenOf(child,selector){
	let element;
	let elements=document.querySelectorAll(selector);
	for(let i=0;i<elements.length;i+=1){
		element=elements[i];
		while (element.childNodes.length > child) {
			element.removeChild(element.lastChild);
		}
	}
}
function DownloadData(data,filename='character.json'){
	let blob=new Blob([data],{type : 'application/json'});
	let url = window.URL.createObjectURL(blob);
	let a = document.createElement('a');
	a.style.display = 'none';
	a.href = url;

	a.download = filename;
	document.body.appendChild(a);
	a.click();
	window.URL.revokeObjectURL(url);
}
function lastTableRow(selector,columnCount=0){
	let rows;
	let cell=document.createElement("td");
	let row=document.createElement("tr");

	for(let i=0;i<columnCount;i+=1){
		row.appendChild(cell);
		cell=document.createElement("td");
	}
	document.querySelector(selector).appendChild(row);
	rows=document.querySelectorAll(selector+" tr");
	
	if(rows.length===1){
		return row;
	}else{
		return rows[rows.length-1];
	}
}
function AddLevelFor(name,callback){
	let element=document.querySelector("#"+name+"-level");

	if(element!==null){
		return;
	}
		
	element=GenerateLevelFor(name,callback);
	document.querySelector("#character-levels").appendChild(element);
}
function GenerateLevelFor(name,callback){
	let label=document.createElement("label");
	let input=document.createElement("input");

	label.textContent=name;

	input.type="number";
	input.min=1;
	input.max=20;
	input.value=1;
	input.id=name+"-level";
	input.classList.add("saveable");
	input.classList.add("class-level");
	input.addEventListener("change",callback);
		
	return WrapIn("li",[label,input]);
}
function WrapIn(tag,elements){
	let result=document.createElement(tag);
	for(let i=0;i<elements.length;i+=1){
		result.appendChild(elements[i]);
	}
	return result;
}
function SelectedContainsValue(val,selector){
	let elem=document.querySelector(selector).selectedOptions;
	for(let i=0;i<elem.length;i+=1){
		if(elem[i].value===val){
			return true;
		}
	}
	return false;
}
function UrlForClass(name){
	let elem=document.querySelector("#class").selectedOptions;
	for(let i=0;i<elem.length;i+=1){
		if(elem[i].text.toLowerCase()===name){
			return elem[i].value;
		}
	}
	return null;
}
function ClearDependant(){
	for(let i=0;i<settings.character_dependant.length;i+=1){
		RemoveChildrenOf(settings.character_dependant[i]);
	}
}

function LoadDialog(dialog){
	let element=document.querySelector("#modal");
	
	element.innerHTML=dialogs[dialog].content;
	dialogs[dialog].postrender();
	
	element.classList.remove("hidden");
}

function MapAbility(ability){
	let result={
		name:ability,
		modifier:"*-mod",
		saving:{
			has_saving:true,
			saving:"*-saving",
			proficiency:"*-proficiency"
		},
		proficiency:{
			has_proficiency:false
		},
		skills:{
			template:"*-?",
			proficiency:"?-proficiency"
		}
	};

	result.saving.saving=mapper.ability.any.saving.saving.replace("*",ability);
	result.saving.proficiency=mapper.ability.any.saving.proficiency.replace("*",ability);
	result.modifier=mapper.ability.any.modifier.replace("*",ability);

	return result;
}

function Distance(from,to){
	let distance={
		x:Math.max(from.x,to.x)-Math.min(from.x,to.x),
		y:Math.max(from.y,to.y)-Math.min(from.y,to.y)
	};
	return distance.x+distance.y;
}
function Connect(url,name,pass){
	$.ajax({
		url: "http://"+url,
		type: 'GET',
		dataType: 'json'//,
		//data:{"name":name,"password":pass}
	}).done((reponse)=>{
		localStorage.setItem("host",url);
	}).fail((err)=>{
		console.error("Could not connect to GM's Host.");
		console.error(err);
		localStorage.removeItem("host");
	});
}

function knownSpellsFrom(level){
	let result=[];
	let item;
	let row;
	let table=document.querySelectorAll("#spell-table .magic-url");

	if(level===0){
		level="Cantrip";
	}else{
		level=level.toString();
	}

	for(let i=0;i<table.length;i+=1){
		if(row.childNodes[1].textContent===level || level==="-1"){
			item={
				name:
					table.
					childNodes[i].
					childNodes[0].
					textContent,
				url:
					table.
					childNodes[i].
					lastChild.
					value
			};
			result.push(item);
		}
	}
	return result;
}
